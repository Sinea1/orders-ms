FROM node:6-alpine

WORKDIR /app

COPY package.json .

RUN npm install

COPY ./src .

EXPOSE 8000

CMD ["npm", "start"]