const express = require('express')
const app = express()

app.get('/', (req, res) => res.send('Hello World!'))

console.log(process.env.PORT)

app.listen(process.env.PORT, () => console.log('Example app listening on port 3000!'))